import React, { useContext, useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom'
import { UserContext } from '../../Store/Store'

import './style.css'
export default function RepoDetails() {
  const {userRepo}=useContext(UserContext)
 
  let {id} = useParams()

  let repoDetails = userRepo[id]
  
  
  return (
    <>
      <nav className='nav-bar'>
        <Link className='link' to='/'>
          Search Box
        </Link>
        <Link className='link' to='/repo'>
          Repositories list
        </Link>
      </nav>
    <div className="repoDetail-Container" >
      <div className="repoDetail-wrapper">
            <div className="repoDetail-left">
            <img className="repoDetail-avatar" src={repoDetails.owner.avatar_url} alt='not available'/>
            </div>
            <div className="repoDetail-right">
            <h2 className="repo-title-name">{repoDetails.name}</h2>
            <span>{repoDetails.description}</span>
            </div>
          </div>
    </div>
    </>
  )
}
