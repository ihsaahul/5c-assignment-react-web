import axios from "axios";
import React, { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../../Store/Store";

import "./style.css";
export default function Search() {
  const { setUserRepo,searchUser,setSearchUser } = useContext(UserContext);

  const [name, setName] = useState("");

  const navigate = useNavigate();

  const handleChange = (e) => {
    setName(e.target.value);
  };

  const handleSubmit = async () => {
     if(searchUser[name]){
      setUserRepo(searchUser[name])
      navigate("/repo");
     }else{
       let res = await axios.get(`https://api.github.com/users/${name}/repos`);
       setSearchUser({...searchUser,[name]:res.data})
       setUserRepo(res.data);
       navigate("/repo");
     }
  };

  return (
    <div className='search-box'>
      <div className='search-wrapper'>
        <input
          className='search-input'
          type='text'
          name='user'
          placeholder='User Name'
          value={name}
          onChange={(e) => handleChange(e)}
        />
        <button onClick={handleSubmit} className='submit-btn'>
          Search
        </button>
      </div>
    </div>
  );
}
