import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { UserContext } from "../../Store/Store";
import "./style.css";
export default function Followers() {
  const { userFollowers, setUserRepo } = useContext(UserContext);

  const navigate = useNavigate();

  console.log(userFollowers);

  const handleClick = async (url) => {
    let res = await axios.get(url);
    setUserRepo(res.data);
    navigate("/repo");
  };

  return (
    <>
    <nav className='nav-bar'>
        <Link className='link' to='/'>
          Search Box
        </Link>
        <Link className='link' to='/repo'>
          Repositories list
        </Link>
      </nav>
    <div>
      <h1 className='followers-title'>Followers</h1>

      <div className='followers-container'>
        {userFollowers?.map((obj, i) => (
          <div key={i} className='followers-name'>
            <div className='followers-left'>
              <img
                className='followers-avatar'
                src={obj.avatar_url}
                alt='not available'
              />
            </div>
            <div className='followers-center'>
              <h4
                className='followers-title-name'
                onClick={() => handleClick(obj.repos_url)}
              >
                {obj.login}
              </h4>
              <span>{obj.description}</span>
            </div>
          </div>
        ))}
      </div>
    </div>
    </>
  );
}
