import React, { useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import UserInfo from "../../component/user-info/UserInfo";
import { UserContext } from "../../Store/Store";
import "./style.css";

export default function RepoList() {
  const { userRepo } = useContext(UserContext);
  const navigate = useNavigate();

  return (
    <div className='repo-container'>
      <nav className='nav-bar'>
        <Link className='link' to='/'>
          Serach Box
        </Link>
      </nav>
      <UserInfo />
      <h1 className='repo-title'>Repositories List</h1>
      <div className='repo-list-center'>
        <div className='repo-wrap'>
          <div className='repositories-container'>
            {userRepo?.map((obj, index) => (
              <div key={index} className='repo-name'>
                <div className='repo-left'>
                  <img
                    className='repo-avatar'
                    src={obj.owner.avatar_url}
                    alt='not available'
                  />
                </div>
                <div className='repo-center'>
                  <h4
                    className='repo-title-name'
                    onClick={() => navigate(`/${index}`)}
                  >
                    {obj.name}
                  </h4>
                  <span>{obj.description}</span>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
