import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../../Store/Store";
import "./style.css";
export default function UserInfo() {
  let { userRepo, setUserFollowers } = useContext(UserContext);
  const [userInfo, setUserInfo] = useState({});
  const navigate = useNavigate();

  console.log(userRepo);
  const fetchUserInfo = async () => {
    let res = await axios.get(userRepo[0]?.owner.url);
    setUserInfo(res.data);
  };

  const handleFollowers = async () => {
    let res = await axios.get(userRepo[0]?.owner.followers_url);
    setUserFollowers(res.data);
    navigate("/followers");
  };
  console.log(userInfo);
  useEffect(() => {
    fetchUserInfo();
  }, []);
  return (
    <div className='user-info-wrapper'>
      <div className='user-pic'>
        <img
          className='user-avatar'
          src={userRepo[0]?.owner.avatar_url}
          alt='error'
        />
      </div>
      <div className='user-detail-info'>
        <h2>user name: {userRepo[0]?.owner.login}</h2>
        <h4>owner name: {userInfo.name}</h4>
        <h4>Bio : {userInfo.bio}</h4>
        <h4>Followers : {userInfo.followers}</h4>
        <h4>Following : {userInfo.following}</h4>
      </div>
      <div>
        <button onClick={handleFollowers}>Followers</button>
      </div>
    </div>
  );
}
