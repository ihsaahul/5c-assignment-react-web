import React, { useContext } from "react";
import { Navigate, Outlet } from "react-router-dom";
import { UserContext } from "../../Store/Store";

const UserRoute = () => {
  const { userRepo } = useContext(UserContext);

  return userRepo ? <Outlet /> : <Navigate to='/404' />;
};

export default UserRoute;
