import { useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import UserRoute from "./component/custom-route/UserRoute";
import PageNotFound from "./Pages/404/PageNotFound";
import Followers from "./Pages/Followers/Followers";
import RepoDetails from "./Pages/Repo-descr/RepoDetails";
import RepoList from "./Pages/RepoList/RepoList";
import Search from "./Pages/search/Search";

import StoreProvider from "./Store/Store";

function App() {
  const [searchUser,setSearchUser]=useState({})
  const [userRepo, setUserRepo] = useState([]);
  const [userFollowers, setUserFollowers] = useState([]);
  console.log('searchUser',searchUser)
  return (
    <StoreProvider
      userData={{ userRepo, setUserRepo, userFollowers, setUserFollowers ,searchUser,setSearchUser}}
    >
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Search />} />
          <Route path='/' element={<UserRoute />}>
            <Route path='repo' element={<RepoList />} />
            <Route path=':id' element={<RepoDetails />} />
            <Route path='followers' element={<Followers />} />
          </Route>
          <Route path='404' element={<PageNotFound />} />
        </Routes>
      </BrowserRouter>
    </StoreProvider>
  );
}

export default App;
