import React, { createContext } from "react";

export const UserContext = createContext({});

export default function StoreProvider({ children, userData }) {
  return (
    <UserContext.Provider value={userData}>{children}</UserContext.Provider>
  );
}
